<h2><?php _e('My Addresses'); ?></h2>

<?php

		
		echo '<form action="" method="post" id="address_form">';
		if ( ! empty( $otherAddr ) ) {
			echo '<div id="addresses">';

			foreach ( $otherAddr as $idx => $address ) {
				$wma_current_address = $address;
				echo '<div class="shipping_address address_block" id="shipping_address_' . $idx . '">';
				echo '<p align="right"><a href="#" class="delete">' . __( 'delete', $plugin_slug ) . '</a></p>';

				do_action( 'woocommerce_before_checkout_shipping_form', $checkout );

        $label['id'] = 'label';
        $label['label'] = __( 'Label', $plugin_slug );
        woocommerce_form_field( 'label[]', $label, $address['label'] );

				foreach ( $shipFields as $key => $field ) {

					if ( 'shipping_alt' == $key ) {
						continue;
					}

					$val = '';
					if ( isset( $address[ $key ] ) ) {
						$val = $address[ $key ];
					}

					$field['id'] = $key;
					$key .= '[]';
					woocommerce_form_field( $key, $field, $val );
				}

				if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) {
					$is_checked = $address['shipping_address_is_default'] == 'true' ? "checked" : "";
					echo '<input type="checkbox" class="default_shipping_address" ' . $is_checked . ' value="' . $address['shipping_address_is_default'] . '"> ' . __( 'Mark this shipping address as default', $plugin_slug );
					echo '<input type="hidden" class="hidden_default_shipping_address" name="shipping_address_is_default[]" value="' . $address['shipping_address_is_default'] . '" />';
				}

				do_action( 'woocommerce_after_checkout_shipping_form', $checkout );
				echo '</div>';
			}
			echo '</div>';
		} else {

			echo '<div id="addresses">';

			foreach ( $shipFields as $key => $field ) :
				$field['id'] = $key;
				$key .= '[]';
				woocommerce_form_field( $key, $field, $checkout->get_value( $field['id'] ) );
			endforeach;

			if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) {
				echo '<input type="checkbox" class="default_shipping_address" checked value="true"> ' . __( 'Mark this shipping address as default', $plugin_slug );
				echo '<input type="hidden" class="hidden_default_shipping_address" name="shipping_address_is_default[]" value="true" />';
			}

			echo '</div>';
		}
		echo '<div class="form-row">
                <input type="hidden" name="shipping_account_address_action" value="save" />
                <input type="submit" name="set_addresses" value="' . __( 'Save Addresses', $plugin_slug ) . '" class="button alt" />
                <a class="add_address" href="#">' . __( 'Add another', $plugin_slug ) . '</a>
            </div>';
		echo '</form>';
		?>
		<script type="text/javascript">
			var tmpl = '<div class="shipping_address address_block"><p align="right"><a href="#" class="delete"><?php _e( "delete", $plugin_slug ); ?></a></p>';

            tmpl += '<?php $label['id'] = 'label';
                $label['label'] = __( 'Label', $plugin_slug );
                $row = woocommerce_form_field( 'label[]', $label, '' );
                echo str_replace("\n", "\\\n", str_replace("'", "\'", $row));
                ?>';

			tmpl += '<?php foreach ($shipFields as $key => $field) :
				if ( 'shipping_alt' == $key ) {
					continue;
				}
				$field['return'] = true;
				$val = '';
				$field['id'] = $key;
				$key .= '[]';
				$row = woocommerce_form_field( $key, $field, $val );
				echo str_replace("\n", "\\\n", str_replace("'", "\'", $row));
			endforeach; ?>';

			<?php if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>
				tmpl += '<input type="checkbox" class="default_shipping_address" value="false"> <?php _e( "Mark this shipping address as default", $plugin_slug ); ?>';
				tmpl += '<input type="hidden" class="hidden_default_shipping_address" name="shipping_address_is_default[]" value="false" />';
			<?php endif; ?>

			tmpl += '</div>';
			jQuery(".add_address").click(function (e) {
				e.preventDefault();

				jQuery("#addresses").append(tmpl);

				jQuery('html,body').animate({
						scrollTop: jQuery('#addresses .shipping_address:last').offset().top},
					'slow');
			});

			jQuery(".delete").live("click", function (e) {
				e.preventDefault();
				jQuery(this).parents("div.address_block").remove();
			});

			jQuery(document).ready(function () {

				jQuery(document).on("click", ".default_shipping_address", function () {
					if (this.checked) {
						jQuery("input.default_shipping_address").not(this).removeAttr("checked");
						jQuery("input.default_shipping_address").not(this).val("false");
						jQuery("input.hidden_default_shipping_address").val("false");
						jQuery(this).next().val('true');
						jQuery(this).val('true');
					}
					else {
						jQuery("input.default_shipping_address").val("false");
						jQuery("input.hidden_default_shipping_address").val("false");
					}
				});

				jQuery("#address_form").submit(function () {
					var valid = true;
					jQuery("input[type=text],select").each(function () {
						if (jQuery(this).prev("label").children("abbr").length == 1 && jQuery(this).val() == "") {
							jQuery(this).focus();
							valid = false;
							return false;
						}
					});
					return valid;
				});
			});
		</script>